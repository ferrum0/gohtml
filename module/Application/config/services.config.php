<?php

namespace Application;

use Application\Factory\ServiceFactory;
use Application\Service\ApplicationState;
use Application\Service\AuthService;
use Application\Service\TextProcessor;
use Doctrine\ORM\EntityManager;
use Zend\ServiceManager\ServiceManager;

return [
    'abstract_factories' => [],
    'aliases' => [],
    'factories' => [
        TextProcessor::class => ServiceFactory::class,
        AuthService::class => ServiceFactory::class,
        ApplicationState::class => ServiceFactory::class
    ],
    'invokables' => [],
    'services' => [
    ],
    'shared' => [],
];