<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Application\Controller\AuthController;
use Application\Controller\IndexController;
use Application\Factory\ControllersFactory;

return [
    'router' => require_once("router.config.php"),
    'controllers' => [
        'factories' => [
            IndexController::class => ControllersFactory::class,
            AuthController::class => ControllersFactory::class,
        ],
        'aliases' => [
            'auth' => AuthController::class,
            'index' => IndexController::class,
        ],
    ],

    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => [
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ],

    'service_manager' => require_once("services.config.php")
];
