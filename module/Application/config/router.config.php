<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 10/19/16
 * Time: 1:25 PM
 */
namespace Application;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Regex;
use Zend\Router\Http\Segment;

return [
    'routes' => [
        'application' => [
            'type'    => Segment::class,
            'options' => [
                'route' => '/api/:controller[/:action]',
                'constraints' => [
                    'controller' => '[a-zA-Z][a-zA-Z0-9_-]+',
                    'action'     => '[a-zA-Z][a-zA-Z0-9_-]+',
                ],
                'defaults' => [
                    'controller' => Controller\IndexController::class,
                    'action'     => 'index',
                ],
            ],
        ],
    ],
];