<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 11/8/16
 * Time: 11:18 PM
 */

namespace Application\Interfaces;


interface Arrayable
{
    function toArray();
}