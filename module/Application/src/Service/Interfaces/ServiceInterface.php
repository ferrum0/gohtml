<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 10/21/16
 * Time: 7:58 PM
 */

namespace Application\Service\Interfaces;


use Doctrine\ORM\EntityManager;

interface ServiceInterface
{
    function __construct(EntityManager $em);

    /**
     * @return EntityManager
     */
    function getEm(): EntityManager;

    /**
     * @param EntityManager $em
     */
    function setEm(EntityManager $em);
}