<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 10/21/16
 * Time: 7:58 PM
 */

namespace Application\Service;


use Application\Service\Interfaces\ServiceInterface;
use Doctrine\ORM\EntityManager;

class AbstractService implements ServiceInterface
{
    /**
     * @var $em EntityManager
     */
    protected $em;

    function __construct(EntityManager $em)
    {
        $this->setEm($em);
    }

    /**
     * @return EntityManager
     */
    public function getEm(): EntityManager
    {
        return $this->em;
    }

    /**
     * @param EntityManager $em
     */
    public function setEm(EntityManager $em)
    {
        $this->em = $em;
    }
}