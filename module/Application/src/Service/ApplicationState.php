<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 10/19/16
 * Time: 1:12 PM
 */

namespace Application\Service;


use Application\Entity\Blog;
use Application\Entity\Lesson;
use Application\Entity\User;
use Application\Interfaces\Arrayable;
use Application\Mechanism\Navigator;
use Application\Mechanism\Paginator;
use Doctrine\ORM\EntityManager;
use Zend\Hydrator\ClassMethods;
use Zend\Session\Container;

class ApplicationState extends AbstractService implements Arrayable
{
    /**
     * @var $blogs Blog[]
     */
    protected $blogs = null;

    /**
     * @var $user User
     */
    protected $user = null;

    /**
     * @var Container
     */
    protected $container = null;

    /**
     * @var Paginator
     */
    protected $paginator = null;

    /**
     * @var Navigator
     */
    protected $navigator = null;

    /**
     * @var Blog
     */
    protected $active_blog = null;

    /**
     * @var string
     */
    protected $order = null;

    /**
     * @var mixed
     */
    protected $action_result;

    const USER_KEY = 'user';

    function __construct(EntityManager $em)
    {
        parent::__construct($em);

        $this->setBlogs(
            $this->getEm()
                ->getRepository(Blog::class)
                ->findAll()
        );

        $this->setContainer(
            new Container("application")
        );

        $this->getLoggedUser();
    }

    /**
     * @return User
     */
    public function getLoggedUser()
    {
        if (!isset($this->user)) {
            $userData =
                $this->sessionLoad(
                    self::USER_KEY
                );

            if (isset($userData)) {
                $this->user = new User();
                $hydrator = new ClassMethods();

                $hydrator->hydrate($userData, $this->user);
            }
        }

        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setLoggedUser($user)
    {
        if ($user instanceof User) {
            $this->user = $user;

            $this->sessionSave(
                self::USER_KEY,
                $user->toArray()
            );

        } elseif (!isset($user)) {

            $this->user = null;

            $this->sessionSave(
                self::USER_KEY,
                null
            );
        }
    }

    /**
     * @return Blog[]
     */
    public function getBlogs(): array
    {
        return $this->blogs;
    }

    /**
     * @param Blog[] $blogs
     */
    public function setBlogs(array $blogs)
    {
        $this->blogs = $blogs;
    }


    public function sessionSave($param, $value)
    {
        $this->getContainer()->$param = $value;
    }

    public function sessionLoad($param)
    {
        if (isset($this->getContainer()->$param)) {
            return $this->getContainer()->$param;
        }

        return null;
    }

    /**
     * @return Container
     */
    public function getContainer(): Container
    {
        return $this->container;
    }

    /**
     * @param Container $container
     */
    public function setContainer(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @return Paginator
     */
    public function getPaginator()
    {
        return $this->paginator;
    }

    /**
     * @param mixed $paginator
     */
    public function setPaginator($paginator)
    {
        $this->paginator = $paginator;
    }

    /**
     * @return Navigator
     */
    public function getNavigator(): Navigator
    {
        return $this->navigator;
    }

    /**
     * @param Navigator $navigator
     */
    public function setNavigator(Navigator $navigator)
    {
        $this->navigator = $navigator;
    }

    /**
     * @return Blog
     */
    public function getActiveBlog()
    {
        return $this->active_blog;
    }

    /**
     * @param mixed $active_blog
     */
    public function setActiveBlog($active_blog)
    {
        $this->active_blog = $active_blog;
    }

    /**
     * @return string
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param string $order
     */
    public function setOrder(string $order)
    {
        $this->order = $order;
    }

    /**
     * @return mixed
     */
    public function getActionResult()
    {
        return $this->action_result;
    }

    /**
     * @param mixed $action_result
     */
    public function setActionResult($action_result)
    {
        $this->action_result = $action_result;
    }

    public function toArray()
    {
        $actionResult = $this->getActionResult();

        if(is_array($actionResult)){
            foreach ($actionResult as $key => $value) {
                if ($actionResult[$key] instanceof Lesson) {
                    $actionResult[$key]->setText("");
                }
            }

            $this->setActionResult($actionResult);
        }

        return
            $this->objectToArray(
                get_object_vars($this)
            );
    }

    protected function objectToArray($obj)
    {
        if (is_object($obj)) {
            if ($obj instanceOf Arrayable) {
                $_arr = $obj->toArray();
            } else {
                $_arr = get_object_vars($obj);
            }
        } else {
            $_arr = $obj;
        }

        $arr = [];

        foreach ($_arr as $key => $val) {
            $val = (is_array($val) || is_object($val)) ? $this->objectToArray($val) : $val;
            $arr[$key] = $val;
        }

        return $arr;
    }
}