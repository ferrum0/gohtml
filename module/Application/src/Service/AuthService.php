<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 10/24/16
 * Time: 8:24 PM
 */

namespace Application\Service;


use Application\Entity\User;
use Zend\Session\Container;

class AuthService extends AbstractService
{
    function authUser($login, $password)
    {
        $user =
            $this->getEm()
                ->getRepository(User::class)
                ->findOneBy([
                    'login' => $login,
                    'password' =>
                        $this->stringToPasswordEncode(
                            $password
                        )
                ]);

        return $user;
    }

    public function stringToPasswordEncode(string $string) : string
    {
        return md5($string);
    }
}