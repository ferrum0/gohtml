<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 10/19/16
 * Time: 1:12 PM
 */

namespace Application\Service;


use Application\Entity\Blog;
use Application\Entity\User;
use Doctrine\ORM\EntityManager;

class TextProcessor extends AbstractService
{
    public function processText(string $text)
    {
        $result = "";
        $text = str_replace('\r\n', PHP_EOL, $text);
        $string = explode("%html%", $text);

        if (count($string) > 1) {
            foreach ($string as $txt) {
                $txt_arr = explode("%/html%", $txt);
                if (count($txt_arr) > 1) {

                    $result .=
                        str_replace(
                            ["&#039;", "&quot;"],
                            ["'", '"'],
                            htmlspecialchars_decode(
                                $txt_arr[0]
                            )
                        );

                    $result .= $this->nl2br($txt_arr[1]);
                } else {
                    $result .= $this->nl2br($txt);
                }
            }
            $text = $result;
        } else {
            $text = $this->nl2br($text);
        }

        return str_replace(
            array(
                "%code%",
                "%code php%",
                "%code sql%",
                "%code css%",
                "%code js%",
                "%/code%",
                "%b%",
                "%/b%"
            ),
            array(
                "<prism class=\"language-html\">",
                "<prism class=\"language-php\">",
                "<prism class=\"language-sql\">",
                "<prism class=\"language-css\">",
                "<prism>",//;this.innerHTML = Prism.highlight(this.innerHTML, Prism.languages.javascript)
                "</prism>",
                "<h2>",
                "</h2>"
            ),
            $text
        );
    }

    function nl2br(string $string): string
    {
        return str_replace(PHP_EOL, PHP_EOL . "<br />", $string);
    }

    function translitLower($string)
    {
        $tr = [
            "а" => "a", "б" => "b", "в" => "v", "г" => "g",
            "д" => "d", "е" => "e", "ж" => "j", "з" => "z",
            "и" => "i", "й" => "y", "к" => "k", "л" => "l",
            "м" => "m", "н" => "n", "о" => "o", "п" => "p",
            "р" => "r", "с" => "s", "т" => "t", "у" => "u",
            "ф" => "f", "х" => "h", "ц" => "ts", "ч" => "ch",
            "ш" => "sh", "щ" => "sch", "ъ" => "y", "ы" => "yi",
            "ь" => "", "э" => "e", "ю" => "yu", "я" => "ya",
            "," => "", " " => "-", "." => "", "+" => "plus",
            "-" => "minus", "\"" => "", "'" => "", "/" => "",
            "\\" => "", "/*" => ""
        ];

        return
            strtr(
                mb_strtolower(
                    $string, 'UTF-8'
                ),
                $tr
            );
    }
}