<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 10/18/16
 * Time: 12:46 PM
 */

namespace Application\Entity;

use Application\Interfaces\Arrayable;
use Doctrine\ORM\Mapping AS ORM;

abstract class AbstractEntity implements Arrayable
{
    use \Application\Traits\Arrayable;

    /**
     * @ORM\Id
     * @ORM\Column
     * @ORM\GeneratedValue
     * @var $id int
     */
    protected $id;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
}