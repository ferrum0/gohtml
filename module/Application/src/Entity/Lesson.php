<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 10/16/16
 * Time: 4:44 PM
 */

namespace Application\Entity;

use Doctrine\ORM\Mapping AS ORM;

/**
 * Class Application\Entity\Lesson
 * @ORM\Entity
 * @ORM\Table(name="lessons")
 */
class Lesson extends AbstractEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="author")
     */
    protected $author;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $date;

    /**
     * @ORM\ManyToMany(targetEntity="Tag")
     * @ORM\JoinTable(
     *     name="lessons_tags",
     *     joinColumns={@ORM\JoinColumn(name="lesson", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="tag", referencedColumnName="id")}
     * )
     */
    protected $tags;

    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="lesson")
     */
    protected $comments;

    /**
     * @ORM\OneToMany(targetEntity="Vote", mappedBy="lesson")
     */
    protected $votes;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $views;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $short_text;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $confirmed;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $difficulty;

    /**
     * @ORM\Column(type="string")
     * @var $name string
     */
    protected $name;

    /**
     * @ORM\Column(type="string")
     * @var $text string
     */
    protected $text;

    /**
     * @ORM\Column(type="string")
     * @var $text string
     */
    protected $avatar;

    /**
     * @var string
     */
    protected $url;

    const PUBLISHED = 1;
    const NOT_PUBLISHED = 0;

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getAvatar(): string
    {
        return $this->avatar;
    }

    /**
     * @param string $avatar
     */
    public function setAvatar(string $avatar)
    {
        $this->avatar = $avatar;
    }

    /**
     * @return User
     */
    public function getAuthor(): User
    {
        return $this->author;
    }

    /**
     * @param User $author
     */
    public function setAuthor(User $author)
    {
        $this->author = $author;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate(string $date)
    {
        $this->date = $date;
    }

    /**
     * @return Tag[]
     */
    public function getTags():array
    {
        return
            $this->tags
                ?$this->tags->toArray()
                :[];
    }

    /**
     * @param Tag[] $tags
     */
    public function setTags(array $tags)
    {
        $this->tags = $tags;
    }

    /**
     * @return string
     */
    public function getViews(): string
    {
        return $this->views;
    }

    /**
     * @param string $views
     */
    public function setViews(string $views)
    {
        $this->views = $views;
    }

    /**
     * @return string
     */
    public function getShortText(): string
    {
        return $this->short_text;
    }

    /**
     * @param string $short_text
     */
    public function setShortText(string $short_text)
    {
        $this->short_text = $short_text;
    }

    /**
     * @return string
     */
    public function getConfirmed(): string
    {
        return $this->confirmed;
    }

    /**
     * @param string $confirmed
     */
    public function setConfirmed(string $confirmed)
    {
        $this->confirmed = $confirmed;
    }

    /**
     * @return string
     */
    public function getDifficulty(): string
    {
        return $this->difficulty;
    }

    /**
     * @param string $difficulty
     */
    public function setDifficulty(string $difficulty)
    {
        $this->difficulty = $difficulty;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param mixed $comments
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    }

    /**
     * @return mixed
     */
    public function getLikes()
    {
        return $this->likes;
    }

    /**
     * @param mixed $likes
     */
    public function setLikes($likes)
    {
        $this->likes = $likes;
    }
}