<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 10/16/16
 * Time: 4:44 PM
 */

namespace Application\Entity;

use Doctrine\ORM\Mapping AS ORM;

/**
 * Class Application\Entity\User
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User extends AbstractEntity
{
    /**
     * @ORM\Column(type="string")
     * @var $login string
     */
    protected $login;

    /**
     * @ORM\Column(type="string")
     * @var $password string
     */
    protected $password;

    /**
     * @ORM\Column(type="string")
     * @var $name string
     */
    protected $name;

    /**
     * @ORM\Column(type="string")
     * @var $salt string
     */
    protected $salt;

    /**
     * @ORM\Column(type="string")
     * @var $group string
     */
    protected $group;

    /**
     * @ORM\Column(type="string")
     * @var $mail string
     */
    protected $mail;

    /**
     * @ORM\Column(type="integer")
     * @var $tries_count integer
     */
    protected $tries_count;

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin(string $login)
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSalt(): string
    {
        return $this->salt;
    }

    /**
     * @param string $salt
     */
    public function setSalt(string $salt)
    {
        $this->salt = $salt;
    }

    /**
     * @return string
     */
    public function getGroup(): string
    {
        return $this->group;
    }

    /**
     * @param string $group
     */
    public function setGroup(string $group)
    {
        $this->group = $group;
    }

    /**
     * @return string
     */
    public function getMail(): string
    {
        return $this->mail;
    }

    /**
     * @param string $mail
     */
    public function setMail(string $mail)
    {
        $this->mail = $mail;
    }

    /**
     * @return int
     */
    public function getTriesCount(): int
    {
        return $this->tries_count;
    }

    /**
     * @param int $tries_count
     */
    public function setTriesCount(int $tries_count)
    {
        $this->tries_count = $tries_count;
    }
}