<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 10/16/16
 * Time: 4:44 PM
 */

namespace Application\Entity;

use Doctrine\ORM\Mapping AS ORM;

/**
 * Class Application\Entity\Vote
 * @ORM\Entity
 * @ORM\Table(name="votes")
 */
class Vote extends AbstractEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="Lesson")
     * @ORM\JoinColumn(name="lesson")
     */
    protected $lesson;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user")
     */
    protected $user;

    /**
     * @ORM\Column(name="value", type="integer")
     * @var integer
     */
    protected $type;

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getLesson()
    {
        return $this->lesson;
    }

    /**
     * @param mixed $lesson
     */
    public function setLesson($lesson)
    {
        $this->lesson = $lesson;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }
}