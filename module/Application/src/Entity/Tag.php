<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 10/16/16
 * Time: 4:44 PM
 */

namespace Application\Entity;

use Doctrine\ORM\Mapping AS ORM;

/**
 * Class Application\Entity\Tag
 * @ORM\Entity
 * @ORM\Table(name="tags")
 */
class Tag extends AbstractEntity
{
    /**
     * @ORM\Column(type="string")
     * @var $text string
     */
    protected $text;

    /**
     * @ORM\Column(type="string")
     * @var $en_name string
     */
    protected $en_name;

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getEnName(): string
    {
        return $this->en_name;
    }

    /**
     * @param string $en_name
     */
    public function setEnName(string $en_name)
    {
        $this->en_name = $en_name;
    }
}