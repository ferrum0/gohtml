<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 10/16/16
 * Time: 4:44 PM
 */

namespace Application\Entity;

use Doctrine\ORM\Mapping AS ORM;

/**
 * Class Application\Entity\Comment
 * @ORM\Entity
 * @ORM\Table(name="comments")
 */
class Comment extends AbstractEntity
{
    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $text;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user")
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="Lesson")
     * @ORM\JoinColumn(name="lesson")
     */
    protected $lesson;

    /**
     * @ORM\Column(type="date")
     * @var \DateTime
     */
    protected $date;

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return int
     */
    public function getIdLesson(): int
    {
        return $this->id_lesson;
    }

    /**
     * @param int $id_lesson
     */
    public function setIdLesson(int $id_lesson)
    {
        $this->id_lesson = $id_lesson;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function getPlus(): int
    {
        return $this->plus;
    }

    /**
     * @param int $plus
     */
    public function setPlus(int $plus)
    {
        $this->plus = $plus;
    }

    /**
     * @return int
     */
    public function getMinus(): int
    {
        return $this->minus;
    }

    /**
     * @param int $minus
     */
    public function setMinus(int $minus)
    {
        $this->minus = $minus;
    }
}