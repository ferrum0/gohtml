<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 10/26/16
 * Time: 9:13 PM
 */

namespace Application\Mechanism;


use Application\Entity\Blog;
use Application\Entity\Lesson;
use Application\Interfaces\Arrayable;

class Navigator implements Arrayable
{
    use \Application\Traits\Arrayable;

    /**
     * @var Lesson
     */
    protected $lesson;

    /**
     * @return Lesson
     */
    public function getLesson()
    {
        return $this->lesson;
    }

    /**
     * @param Lesson $lesson
     */
    public function setLesson(Lesson $lesson)
    {
        $this->lesson = $lesson;
    }
}