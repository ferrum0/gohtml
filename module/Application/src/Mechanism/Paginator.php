<?php

/**
 * Created by PhpStorm.
 * User: e
 * Date: 10/26/16
 * Time: 9:11 PM
 */

namespace Application\Mechanism;

use Application\Interfaces\Arrayable;

class Paginator implements Arrayable
{
    use \Application\Traits\Arrayable;

    /**
     * @var integer
     */
    protected $count_pages = null;

    /**
     * @var integer
     */
    protected $active_page = null;

    /**
     * @return int
     */
    public function getCountPages(): int
    {
        return $this->count_pages;
    }

    /**
     * @param int $count_pages
     */
    public function setCountPages(int $count_pages)
    {
        $this->count_pages = $count_pages;
    }

    /**
     * @return int
     */
    public function getActivePage(): int
    {
        return $this->active_page;
    }

    /**
     * @param int $active_page
     */
    public function setActivePage(int $active_page)
    {
        $this->active_page = $active_page;
    }
}