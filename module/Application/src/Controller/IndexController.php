<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Entity\Blog;
use Application\Entity\Lesson;
use Application\Entity\Tag;
use Application\Mechanism\Navigator;
use Application\Mechanism\Paginator;
use Application\Service\TextProcessor;
use Doctrine\ORM\QueryBuilder;
use Interop\Container\ContainerInterface;
use Zend\Mvc\Controller\Plugin\Params;
use Zend\View\Helper\ViewModel;
use Zend\View\Model\JsonModel;

class IndexController extends AbstractController
{
    const LIST_COUNT = 20;
    const DEFAULT_TAG = 'js';
    const DEFAULT_PAGE = 1;
    const DEFAULT_ORDER = 'default';

    const ORDERS = [
        self::DEFAULT_ORDER => [
            'field' => 'date',
            'sort' => 'desc'
        ],
        'first-popular' => [
            'field' => 'views',
            'sort' => 'desc'
        ],
        'first-easy' => [
            'field' => 'difficulty',
            'sort' => 'asc'
        ],
        'first-hard' => [
            'field' => 'difficulty',
            'sort' => 'desc'
        ]
    ];

    protected $DEFAULT_TAGS;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->DEFAULT_TAGS =
            $this->getEm()
                ->getRepository(Tag::class)
                ->findAll();
    }

    public function indexAction($requestParams)
    {
        return null;
    }

    public function lessonListAction($requestParams)
    {
        $page = $requestParams['page'] ?: self::DEFAULT_PAGE;

        $findOrder =
            isset($requestParams['order'])
                ? self::ORDERS[$requestParams['order']]
                : self::ORDERS[self::DEFAULT_ORDER];

        $tags =
            array_map(
                function ($tag) {
                    return (
                    $tag instanceof Tag
                        ? $tag->getId()
                        : $tag
                    );
                },
                $requestParams['tags'] ?? $this->DEFAULT_TAGS
            );

        /**
         * @var $textProcessor TextProcessor
         */
        $textProcessor =
            $this->getContainer()
                ->get(TextProcessor::class);

        $qb =
            $this->getEm()
                ->createQueryBuilder();
        /**
         * @var Lesson[] $lessons
         */
        $lessons =
            $qb
                ->select('l,t')
                ->from(Lesson::class, 'l')
                ->join('l.tags', 't')
                ->where(
                    $qb->expr()->andX(
                        $qb->expr()->eq('l.confirmed', Lesson::PUBLISHED),
                        $qb->expr()->eq(
                            't.id',
                            $qb->expr()->any(
                                $this->getEm()
                                    ->createQueryBuilder()
                                    ->select('tags')
                                    ->from(Tag::class, 'tags')
                                    ->where(
                                        $qb->expr()->in('tags.id', $tags)
                                    )
                            )
                        )
                    )
                )
                ->orderBy('l.' . $findOrder['field'], $findOrder['sort'])
                ->setFirstResult(self::LIST_COUNT * ($page - 1))
                //->setMaxResults(self::LIST_COUNT)
                ->getQuery()
                ->getResult();

        $result = [];

        foreach ($lessons as $lesson) {
            $lesson->setShortText(
                $textProcessor->processText(
                    $lesson->getShortText()
                )
            );

            $tags =
                array_map(
                    function (Tag $e) {
                        return $e->toArray();
                    },
                    $lesson->getTags()
                );

            $lesson->setUrl(
                sprintf(
                    '/%s/%d-%s',

                    isset($tags[0])
                        ? $tags[0]['en_name']
                        : self::DEFAULT_TAG,

                    $lesson->getId(),
                    $textProcessor->translitLower(
                        $lesson->getName()
                    )
                )
            );

            $author = $lesson->getAuthor()->toArray();
            $lesson = $lesson->toArray();

            unset($author['password']);
            unset($author['salt']);
            unset($lesson['text']);
            unset($author['mail']);

            $result[] =
                array_merge(
                    $lesson,
                    [
                        'tags_count' => count($lesson['tags']),
                        'tags' => $tags,
                        'author' => $author
                    ]
                );
        }

        return $result;
    }

    public function lessonAction($requestParams)
    {
        $lessonId = $requestParams['id'];

        /**
         * @var $textProcessor TextProcessor
         */
        $textProcessor =
            $this->getContainer()
                ->get(TextProcessor::class);

        /**
         * @var $lesson Lesson
         */
        $lesson =
            $this->getEm()
                ->getRepository(Lesson::class)
                ->find($lessonId);

        $blog = $lesson->getTags();
        $author = $lesson->getAuthor()->toArray();

        unset($author['password']);
        unset($author['salt']);
        unset($author['mail']);

        $result[] =
            array_merge(
                $lesson->toArray(),
                [
                    'blog' => $blog,
                    'author' => $author
                ]
            );

        return $result;
    }
}

