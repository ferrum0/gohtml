<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 10/16/16
 * Time: 11:01 PM
 */

namespace Application\Controller;


use Application\Helpers\JsonSerializer;
use Application\Interfaces\Arrayable;
use Application\Service\ApplicationState;
use Doctrine\DBAL\Schema\View;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Exception\DomainException;
use Zend\Mvc\MvcEvent;
use Zend\View\Helper\Partial;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ModelInterface;
use Zend\View\Model\ViewModel;

abstract class AbstractController extends AbstractActionController
{
    protected $em;
    protected $container;

    function __construct(ContainerInterface $container)
    {
        $em = $container->get(EntityManager::class);

        $this->setContainer($container);
        $this->setEm($em);
    }

    /**
     * Execute the request
     *
     * @param  MvcEvent $e
     * @return mixed
     * @throws DomainException
     */
    public function onDispatch(MvcEvent $e)
    {
        $routeMatch = $e->getRouteMatch();
        if (!$routeMatch) {
            /**
             * @todo Determine requirements for when route match is missing.
             *       Potentially allow pulling directly from request metadata?
             */
            throw new DomainException('Missing route matches; unsure how to retrieve action');
        }

        $action = $routeMatch->getParam('action', 'not-found');
        $method = static::getMethodFromAction($action);

        if (!method_exists($this, $method)) {
            $method = 'notFoundAction';
        }

        $params = array_merge(
            $this->params()->fromQuery(),
            $this->params()->fromPost()
        );

        $actionResult = $this->$method($params);

        $e->setResult(new JsonModel($actionResult));

        return $actionResult;
    }

    /**
     * @return mixed
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param mixed $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * @return EntityManager
     */
    public function getEm(): EntityManager
    {
        return $this->em;
    }

    /**
     * @param EntityManager $em
     */
    public function setEm(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @return ApplicationState
     */
    public function getApplicationState()
    {
        return
            $this->getContainer()
                ->get(ApplicationState::class);
    }
}