<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 10/23/16
 * Time: 11:23 AM
 */

namespace Application\Controller;

use Application\Entity\User;
use Application\Service\ApplicationState;
use Application\Service\AuthService;
use Zend\View\Model\JsonModel;

class AuthController extends AbstractController
{
    public function loginAction($params)
    {
        $user = null;

        if (isset($params['login']) && isset($params['password'])) {
            /**
             * @var $authServise AuthService
             */
            $authServise =
                ->findOneBy([
                $this->getContainer()
                    ->get(AuthService::class);

            $user =
                $authServise->authUser(
                    $params['login'],
                    $params['password']
                );

            if($user instanceof User){
                /**
                 * @var $appState ApplicationState
                 */
                $appState =
                    $this->getContainer()
                        ->get(ApplicationState::class);

                $appState->setLoggedUser($user);
                $user = $user->toArray();
            }
        }

        return new JsonModel([
            'actionResult' => $user
        ]);
    }

    public function logoutAction($params)
    {
        /**
         * @var $appState ApplicationState
         */
        $appState =
            $this->getContainer()
                ->get(ApplicationState::class);

        $appState->setLoggedUser(null);

        return $this->redirect()->toUrl('/');
    }
}
