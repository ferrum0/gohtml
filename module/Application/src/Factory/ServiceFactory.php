<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 10/21/16
 * Time: 8:15 PM
 */

namespace Application\Factory;

use Application\Service\TextProcessor;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ServiceFactory implements FactoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new $requestedName(
            $container->get(EntityManager::class)
        );
    }
}