<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 11/8/16
 * Time: 11:41 PM
 */

namespace Application\Traits;


trait Arrayable
{
    public function toArray($keys = [])
    {
        $vars = get_object_vars($this);

        if ($keys) {
            $result = [];

            foreach ($keys as $key) {
                $result[$key] = $vars[$key];
            }

            return $result;
        } else {
            return $vars;
        }
    }
}