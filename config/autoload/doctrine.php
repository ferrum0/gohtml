<?php

return
    [
        'doctrine' => [
            'driver' => array(
                'application_entities' => [
                    'class' => \Doctrine\ORM\Mapping\Driver\AnnotationDriver::class,
                    'cache' => 'array',
                    'paths' => [__DIR__ . '/../../module/Application/src/Entity']
                ],
                'orm_default' => array(
                    'drivers' => array(
                        'Application\Entity' => 'application_entities',
                    )
                ),
            ),
            'connection' => [
                'orm_default' => [
                    'driverClass' => \Doctrine\DBAL\Driver\PDOPgSql\Driver::class,
                    'params' => [
                        'host'     => 'localhost',
                        'port'     => '5432',
                        'user'     => 'root',
                        'password' => '',
                        'dbname'   => 'gohtml',
                    ]
                ]
            ],
        ],
    ];
