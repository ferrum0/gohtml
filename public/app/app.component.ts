import {Component} from '@angular/core';
import {LessonService, Sort, Tag, Lesson} from './lesson.service';

@Component({
    selector: 'my-app',
    templateUrl: `/templates/app.component.html`
})

export class AppComponent {
    activeLesson: Lesson;
    activeLessonSubscription: any;
    sortSelected: number = 0;

    sorts: Array<Sort> = [
        {field: 'id', name: 'Поновее', order: -1},
        {field: 'views', name: 'Популярное', order: -1},
        {field: 'difficulty', name: 'Посложенее', order: -1},
    ];

    tags: Array<Tag> = [
        {id: 1, name: 'JS', visible: true},
        {id: 2, name: 'CSS', visible: true},
        {id: 3, name: 'HTML', visible: true},
        {id: 4, name: 'PHP', visible: true},
        {id: 5, name: 'MySQL', visible: true}
    ];

    constructor(private _lessonService: LessonService) {
    }

    ngOnInit() {
        this._lessonService.setFilterTags(this.tags);
        this._lessonService.setSort(this.sorts[this.sortSelected]);

        this.activeLessonSubscription =
            this._lessonService.activeLessonSubject.subscribe(
                (lesson: Lesson) => {
                    console.log("activeLesson", lesson);
                    this.activeLesson = lesson;
                }
            );
    }

    ngOnDestroy() {
        this.activeLessonSubscription.unsubscribe();
    }

    public changeSort(index: number = 0) {
        this.sortSelected = index;

        this._lessonService.setSort(this.sorts[this.sortSelected]);
    }

    public setTagVisibility(tag: Tag, value: boolean) {
        tag.visible = value;

        this._lessonService.setFilterTags(this.tags);
    }

    public getTagById = function (id: number) {
        for (var tag of this.tags) {
            if (tag.id === id) {
                return tag;
            }
        }

        return null;
    }
}
