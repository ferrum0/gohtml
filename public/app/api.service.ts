import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Rx';

@Injectable()
export class ApiService {
    constructor(private _http: Http) {
    }

    public get(url: string, callback: any = () => {}): Observable<Response> {
        let request = this._http.get(url);

        //Somewhy doesnt work
        //request.map(response => response.json());

        request.subscribe(callback, ApiService.handleError);

        return request;
    }

    public static handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}