import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import 'rxjs/add/operator/map';
import {Subject} from 'rxjs/Subject';
import {Observable} from "rxjs";
import {Response} from "@angular/http";

export interface Lesson {
    id: number;
    name: string;
    text: string;
    author: any;
    difficulty: number;
    avatar: string;
    tags: any;
    comments: any;
    confirmed: boolean;
    date: string;
    votes: number;
    short_text: string;
    url: string;
    views: number;
}

export interface Sort {
    field: string;
    name: string;
    order: number;
}

export interface Filter {
    tags: Array<Tag>;
}

export interface Tag {
    id: number;
    name: string;
    visible: boolean;
}

@Injectable()
export class LessonService {
    sortSubject: Subject<Sort> = new Subject<Sort>();
    filterSubject: Subject<Filter> = new Subject<Filter>();
    activeLessonSubject: Subject<Lesson> = new Subject<Lesson>();
    lessonsSubject: Subject<Lesson[]> = new Subject<Lesson[]>();

    sort: Sort = null;
    filter: Filter = null;
    activeLesson: Lesson = null;
    lessons: Lesson[] = [];

    constructor(private _apiService: ApiService) {
    }

    public getLessons(): any {
        if (this.lessons != null && this.lessons.length > 0) {
            return {
                subscribe: (callback) => {
                    callback(this.lessons)
                }
            };
        }

        return this.loadMoreLessons();
    }

    public loadMoreLessons() {
        console.log('Load more');

        return this._apiService.get(
            '/api/index/lesson-list',
            (response: any) => {
                console.log('Callback load more');
                this.lessons = this.lessons.concat(response.json());
                this.lessonsSubject.next(this.lessons);

                return this.lessons;
            }
        );
    }

    public getLesson(id: number, callback: any = ()=>{}) {
        return this._apiService.get(
            '/api/index/lesson?id=' + id,
            (response: any) => {callback(response.json()[0])}
        );
    }

    public getSort() {
        return this.sort;
    }

    public getFilter() {
        return this.filter;
    }

    public setSort(sort: Sort) {
        this.sort = sort;
        this.sortSubject.next(this.sort);
    }

    public setFilterTags(tags: Array<Tag>) {
        this.filter = {tags: tags};
        this.filterSubject.next(this.filter);
    }

    public setActiveLesson(lesson: any) {
        this.activeLesson = lesson;
        this.activeLessonSubject.next(this.activeLesson);
    }
}
