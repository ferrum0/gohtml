import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {CharactersComponent} from './characters.component';
import {DashboardComponent} from './dashboard.component';
import {LessonListComponent} from './lesson-list.component';
import {LessonComponent} from './lesson.component';
import {PageNotFoundComponent} from './page-not-found.component';

export const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'lesson-list',
    },
    {path: 'dashboard', component: DashboardComponent},
    {path: 'characters', component: CharactersComponent},
    {path: 'lesson-list', component: LessonListComponent},
    {path: 'lesson-list/:sort', component: LessonListComponent},
    {path: ':blog/:id', component: LessonComponent},
    {path: '**', pathMatch: 'full', component: PageNotFoundComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}

export const routedComponents = [
    DashboardComponent,
    CharactersComponent,
    LessonComponent,
    LessonListComponent,
    PageNotFoundComponent
];
