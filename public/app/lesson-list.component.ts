/**
 * Created by e on 11/21/16.
 */

import {Component, OnInit} from '@angular/core';
import {Lesson, Sort, LessonService, Filter} from './lesson.service';
import {PrismService} from "./prism.service";

@Component({
    selector: 'lesson-list',
    templateUrl: '/templates/lesson-list.component.html',
})

export class LessonListComponent implements OnInit {
    lessons: Lesson[] = [];
    filteredLessons: Lesson[] = [];

    sortSubscription: any;
    filterSubscription: any;
    lessonsSubscription: any;

    sort: Sort;
    filter: Filter;

    constructor(private _lessonService: LessonService,
                private _prismService: PrismService) {
    }

    ngOnInit() {
        this.filter = this._lessonService.getFilter();
        this.sort = this._lessonService.getSort();

        this._lessonService.getLessons().subscribe(
            (lessons: any) => {
                this.mergeLessons(lessons);
            }
        );

        this.sortSubscription =
            this._lessonService.sortSubject.subscribe(
                (sort: Sort) => {
                    this.sort = sort;
                    this.sortLessons();
                }
            );

        this.filterSubscription =
            this._lessonService.filterSubject.subscribe(
                (filter: Filter) => {
                    this.filter = filter;
                    this.filterLessons();
                }
            );

        this.lessonsSubscription =
            this._lessonService.lessonsSubject.subscribe(
                (lessons: Lesson[]) => {
                    this.mergeLessons(lessons);
                }
            );
    }

    ngOnDestroy() {
        this.sortSubscription.unsubscribe();
        this.filterSubscription.unsubscribe();
        // this.lessonsSubscription.unsubscribe();
    }


    mergeLessons(lessons: Lesson[]) {
        for (let lesson of lessons) {
            var lessonFound = false;
            for (let existLesson of this.lessons) {
                if (existLesson.id == lesson.id) {
                    lessonFound = true;
                    break;
                }
            }

            if (!lessonFound) {
                this.lessons.push(lesson);
            }
        }

        this.sortLessons();
        this.filterLessons();
    }

    sortLessons() {
        if (this.sort == null || this.lessons == null) return;

        var order = this.sort.order;
        var field = this.sort.field;

        if (order !== 1 && order !== -1) {
            throw new Error("Invalid order");
        }

        var compare = function (a: any, b: any) {
            if (order * a[field] < order * b[field]) {
                return -1;
            } else if (order * a[field] > order * b[field]) {
                return 1;
            }

            return 0;
        };

        this.filteredLessons.sort(compare);
    }

    filterLessons() {
        if (this.filter == null || this.lessons == null) return;

        this.filteredLessons = [];

        for (let lesson of this.lessons) {
            let lessonIsVisible = false;

            for (let tag of this.filter.tags) {
                for (let lessonTag of lesson.tags) {
                    if (tag.id === lessonTag.id && tag.visible) {
                        lessonIsVisible = true;
                        break;
                    }
                }

                if (lessonIsVisible) {
                    this.filteredLessons.push(lesson);
                    break;
                }
            }
        }
    }
}
