/**
 * Created by e on 11/21/16.
 */

import {Component, OnInit, Optional} from '@angular/core';
import {MdTab} from '@angular/material';
import {Router, ActivatedRoute} from '@angular/router';
import {Lesson, LessonService} from './lesson.service';
import {PrismService} from './prism.service';

@Component({
    selector: 'lesson',
    templateUrl: '/templates/lesson.component.html'
})

export class LessonComponent implements OnInit {
    lesson: Lesson;
    links: any = [
        {url: "#1", name: "Глава 1. Зачем это нужно?"},
        {url: "#2", name: "Глава 2. Как это сделать?"},
        {url: "#3", name: "Глава 3. Что дальше?"},
    ];

    constructor(private _lessonService: LessonService,
                private _activatedRoute: ActivatedRoute,
                private _prismService: PrismService) {
    }

    ngOnInit() {
        var id = parseInt(this._activatedRoute.params['value']['id']);

        console.log("Lesson comp init");

        this._lessonService.getLesson(
            id,
            (lesson: any) => {
                this.lesson = lesson;
                this.lesson.text = this._prismService.highlight(lesson.text);

                this._lessonService.setActiveLesson(this.lesson);
            }
        );
    }

    ngOnDestroy() {
        this._lessonService.setActiveLesson(null);
    }

    onSelect(lesson: Lesson) {

    }
}
