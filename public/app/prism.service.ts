/**
 * Created by e on 2/5/17.
 */

import {Injectable} from '@angular/core';
import * as prismjs from '/node_modules/prismjs/prism.js';
import * as prismjs_php from '/node_modules/prismjs/components/prism-php.js';
import * as prismjs_sql from '/node_modules/prismjs/components/prism-sql.js';
import * as prismjs_css from '/node_modules/prismjs/components/prism-css.js';

@Injectable()
export class PrismService {
    public prismjs: any = prismjs;
    public prismjs_php: any = prismjs_php;
    public prismjs_sql: any = prismjs_sql;
    public prismjs_css: any = prismjs_css;

    protected tags =
    {
        "%code%": this.codeProcessing("markup"),
        "%code html%": this.codeProcessing("markup"),
        "%code php%": this.codeProcessing("php"),
        "%code sql%": this.codeProcessing("sql"),
        "%code css%": this.codeProcessing("css"),
        "%code js%": this.codeProcessing("javascript"),
        "%b%": this.textProcessing("%/b%", "<h2>", "</h2>"),
        "%html%": this.htmlProcessing("%/html%"),
    };

    htmlProcessing(endKey) {
        return {
            callback: (text) => {
                return this.htmlspecialchars_decode(text);
            },
            endKey: endKey
        }
    }

    textProcessing(endKey, startDom, endDom) {
        return {
            callback: (text) => {
                return startDom + this.htmlspecialchars_decode(text) + endDom;
            },
            endKey: endKey
        }
    }

    codeProcessing(type: string): any {
        return {
            callback: (text) => {
                return (
                    "<div class='code'><span>" + type + "</span><pre>"
                    + this.prismText(
                        this.htmlspecialchars_decode(text),
                        type
                    )
                    + "</pre></div>"
                );
            },
            endKey: "%/code%"
        }
    }

    prismText(text: string, lang: string): string {
        return this.prismjs.highlight(
            text,
            this.prismjs.languages[lang]
        );
    }

    highlight(text: string): string {
        var result = "";

        text = text.replace(/\\r\\n/g, '\n');
        text = text.replace(/\\\\/g, '\\');

        do {
            var found: any = false;

            for (let key in this.tags) {
                let search = text.search(key);
                if (search > -1 && (!found || found.index > search + key.length)) {
                    found = {
                        index: search + key.length,
                        key: key,
                        endKey: this.tags[key].endKey,
                        end: text.search(this.tags[key].endKey)
                    }
                }
            }

            if (!found) {
                result = text;
                text = text.substring(text.length);

                break;
            }

            var beforeText = text.substring(0, text.search(found.key));

            var innerText =
                this.tags[found.key].callback(
                    text.substring(
                        found.index,
                        found.end
                    )
                );

            text = text.substring(found.end + found.endKey.length);

            result += this.nl2br(beforeText) + innerText;
        } while (text.length);

        return result;
    }

    nl2br(str) {
        return str.replace(/\n/g, '\n<br />', str);
    }

    multipleReplace(arr, text) {
        for (let key in arr) {
            text = text.replace(key, arr[key]);
        }

        return text;
    }

    htmlspecialchars(text) {
        var map = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#039;'
        };

        return text.replace(/[&<>"']/g, function (m) {
            return map[m];
        });
    }

    htmlspecialchars_decode(text) {
        var map = {
            '&amp;': '&',
            '&lt;': '<',
            '&gt;': '>',
            '&quot;': '"',
            '&#039;': "'"
        };

        return text.replace(/(&amp;|&lt;|&gt;|&quot;|&#039;)/g, function (m) {
            return map[m];
        });
    }
}

/*



 $result .= $this->nl2br($txt_arr[1]);
 } else {
 $result .= $this->nl2br($txt);
 }
 }
 $text = $result;
 } else {
 $text = $this->nl2br($text);
 }

 return str_replace(
 array(
 "%code%",
 "%code php%",
 "%code sql%",
 "%code css%",
 "%code js%",
 "%/code%",
 "%b%",
 "%/b%"
 ),
 array(
 "<prism class=\"language-html\">",
 "<prism class=\"language-php\">",
 "<prism class=\"language-sql\">",
 "<prism class=\"language-css\">",
 "<prism>",//;this.innerHTML = Prism.highlight(this.innerHTML, Prism.languages.javascript)
 "</prism>",
 "<h2>",
 "</h2>"
 ),
 $text
 );
 */