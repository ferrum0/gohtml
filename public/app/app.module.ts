import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HttpModule, XHRBackend} from '@angular/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {MaterialModule} from '@angular/material';

import {AppComponent}   from './app.component';
import {TrackScrollDirective}   from './track-scroll.directive';
import {LessonListComponent}   from './lesson-list.component';
import {AppRoutingModule, routedComponents} from './app-routing.module';
import {CharacterService} from './character.service'
import {LessonService} from "./lesson.service";
import {ApiService} from "./api.service";
import {PrismService} from "./prism.service";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        AppRoutingModule,
        NgbModule.forRoot(),
        MaterialModule.forRoot(),
    ],
    declarations: [
        AppComponent,
        LessonListComponent,
        TrackScrollDirective,
        routedComponents
    ],
    providers: [
        CharacterService,
        LessonService,
        ApiService,
        PrismService
    ],
    bootstrap: [AppComponent],
})
export class AppModule {
}
