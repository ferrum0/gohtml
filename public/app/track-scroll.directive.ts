import {HostListener, Directive} from '@angular/core'
import {LessonService} from "./lesson.service";

@Directive({
    selector: 'track-scroll'
})

export class TrackScrollDirective {
    @HostListener('window:scroll', ['$event'])
    track(event:any) {
        var body = event.target.body,
            doc = event.target.documentElement;

        var scrollTopPercent = (
            (body.scrollTop + doc.clientHeight) / body.scrollHeight
        );

        if(scrollTopPercent > 0.75){
            //this._lessonService.loadMoreLessons();
        }
    }

    constructor(private _lessonService:LessonService){}
}
