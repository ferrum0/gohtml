var gulp = require('gulp'),
    path = require('path'),
    Builder = require('systemjs-builder'),
    ts = require('gulp-typescript'),
    sourcemaps = require('gulp-sourcemaps');

var tsProject = ts.createProject('tsconfig.json');

var appDev = 'app'; // where your ts files are, whatever the folder structure in this folder, it will be recreated in the below 'dist/app' folder
var appProd = 'dist';

/** first transpile your ts files */
gulp.task('ts', function () {
    return gulp.src(appDev + '/**/*.ts')
        .pipe(sourcemaps.init({
            loadMaps: true
        }))
        .pipe(tsProject())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(appProd));
});

/** then bundle */
gulp.task('bundle', function () {
    // optional constructor options
    // sets the baseURL and loads the configuration file
    var builder = new Builder('', 'system-config.js');

    /*
     the parameters of the below buildStatic() method are:
     - your transcompiled application boot file (the one wich would contain the bootstrap(MyApp, [PROVIDERS]) function - in my case 'dist/app/boot.js'
     - the output (file into which it would output the bundled code)
     - options {}
     */
    return builder
        .buildStatic(
            appDev + '/main.js',
            appProd + '/bundle.js',
            {
                minify: false,
                sourceMaps: false
            })
        .then(function () {
            console.log('Build complete');
        })
        .catch(function (err) {
            console.log('Build error');
            console.log(err);
        });
});

/** this runs the above in order. uses gulp4 */
//gulp.task('build', gulp.series(['bundle']));

/*
 var gulp = require('gulp');
 var del = require('del');
 var $ = require('gulp-load-plugins')({ lazy: true });
 var lite = require('lite-server');

 var config = {
 build: './dist/build.js',
 plugins: [
 'node_modules/core-js/client/shim.min.js',
 'node_modules/zone.js/dist/zone.js'
 ],
 index: {
 run: 'index.html',
 aot: 'index-aot.html',
 aotgz: 'index-aot-gzip.html',
 jit: 'index-jit.html'
 },
 dest: './dist',
 root: './'
 };

 gulp.task('help', $.taskListing);
 gulp.task('default', ['help']);

 gulp.task('gzip', function () {
 log('gzipping');
 var source = [].concat(config.plugins, config.build);

 return gulp.src(source)
 .pipe($.gzip())
 .pipe(gulp.dest(config.dest));
 });

 gulp.task('copy-aot-gzip', ['gzip', 'clean'], function () {
 log('copy aot gzip');
 return copyIndex(config.index.aotgz);
 });

 gulp.task('copy-aot', ['clean'], function () {
 log('copy aot');
 return copyIndex(config.index.aot);
 });

 function copyIndex(source) {
 return gulp.src(source)
 .pipe($.rename(config.index.run))
 .pipe(gulp.dest(config.root));
 }

 gulp.task('copy-jit', ['clean'], function () {
 log('copy jit');
 return copyIndex(config.index.jit);
 });

 gulp.task('clean', function (done) {
 log('clean');
 del([config.index.run]).then(paths => {
 // console.log('Deleted files and folders:\n', paths.join('\n'));
 done()
 });
 });

 function log(msg) {
 if (typeof (msg) === 'object') {
 for (var item in msg) {
 if (msg.hasOwnProperty(item)) {
 $.util.log($.util.colors.blue(msg[item]));
 }
 }
 } else {
 $.util.log($.util.colors.blue(msg));
 }
 }

 module.exports = gulp;
 */
